from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'system_dalila.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^teste/$','questionario.views.teste'),
    url(r'^questionario/$','questionario.views.questionarios'),
    url(r'^responder/$','questionario.views.responder'),
    url(r'^te/$','questionario.views.te'),
    # url(r'teste/$', 'questionario.views.teste'),
    url(r'logar/$', 'questionario.views.logar'),
    url(r'desloga/$', 'questionario.views.desloga'),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
