from django.contrib import *
from models import *

class alternativaObjetivaInline(admin.StackedInline):
    model = alternativaObjetiva

    def save_model(self,request,obj,form,change):
        obj.save()

    fieldsets = [
        (None, {'fields':['texto']})
        ]
    extra = 0

# # class alternativaSubjetivaInline(admin.StackedInline):
#     model = alternativaSubjetiva
#     def get_readonly_fields(self, request, obj=None):
#         if obj:
#             obj.index = 's'
#             obj.save()
#             return ['texto',]

#         return self.readonly_fields

#     fieldsets = [
        
#         (None, {'fields':['resposta','texto']})
#     ]

#     readonly_fields = ('resposta',)
#     extra = 0

class questionarioAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Questionario', {'fields': ['nome_Questionario','infor_Questionario']})
    ]

class sessionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Sessao', {'fields':['nome_Session','infor_Session']}),
        ('questionario', {'fields':['questionario',]})
    ]

class questaoAdmin(admin.ModelAdmin):
        
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ['enunciado']

        return self.readonly_fields

    fieldsets = [
        ('Questoes', {'fields':['enunciado','resposta_id']}),
        ('Sessao', {'fields':['session']})
    ]
    list_display = ('enunciado','session')
    # inlines = [alternativaSubjetivaInline,alternativaObjetivaInline]
    inlines = [alternativaObjetivaInline]

# class alterCompostaAdmin(admin.ModelAdmin):
#     def get_readonly_fields(self, request, obj=None):
#         if obj:
#             return ['enunciado']

#         return self.readonly_fields

#     fieldsets = [
#         ('alternativa composta', {'fields':['enunciado']}),
#     ]
#     list_display = ('enunciado',)
#     inlines = [SubjetivaInline,ObjetivaInline]


#declaracoes do que ira ser mostrado na tela de admin
admin.site.register(questionario,questionarioAdmin)
admin.site.register(session,sessionAdmin)
admin.site.register(questao,questaoAdmin)
# admin.site.register(alternativaComposta,alterCompostaAdmin)
