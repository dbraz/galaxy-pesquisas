# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'questao.responta_id'
        db.delete_column(u'questionario_questao', 'responta_id')

        # Adding field 'questao.resposta_id'
        db.add_column(u'questionario_questao', 'resposta_id',
                      self.gf('django.db.models.fields.CharField')(max_length=3, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'questao.responta_id'
        db.add_column(u'questionario_questao', 'responta_id',
                      self.gf('django.db.models.fields.CharField')(max_length=3, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'questao.resposta_id'
        db.delete_column(u'questionario_questao', 'resposta_id')


    models = {
        u'questionario.alternativa': {
            'Meta': {'object_name': 'alternativa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'questao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.questao']", 'null': 'True', 'blank': 'True'}),
            'texto': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'questionario.alternativaobjetiva': {
            'Meta': {'object_name': 'alternativaObjetiva', '_ormbases': [u'questionario.alternativa']},
            u'alternativa_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['questionario.alternativa']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'questionario.questao': {
            'Meta': {'object_name': 'questao'},
            'enunciado': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'resposta_id': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.session']"})
        },
        u'questionario.questionario': {
            'Meta': {'object_name': 'questionario'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'infor_Questionario': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            'nome_Questionario': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'questionario.session': {
            'Meta': {'object_name': 'session'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'infor_Session': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            'nome_Session': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'questionario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.questionario']"})
        }
    }

    complete_apps = ['questionario']