# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'alternativaComposta.questao'
        db.add_column(u'questionario_alternativacomposta', 'questao',
                      self.gf('django.db.models.fields.related.ForeignKey')(default='', to=orm['questionario.questao']),
                      keep_default=False)

        # Adding field 'alternativaComposta.enunciado'
        db.add_column(u'questionario_alternativacomposta', 'enunciado',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'alternativaComposta.questao'
        db.delete_column(u'questionario_alternativacomposta', 'questao_id')

        # Deleting field 'alternativaComposta.enunciado'
        db.delete_column(u'questionario_alternativacomposta', 'enunciado')


    models = {
        u'questionario.alternativa': {
            'Meta': {'object_name': 'alternativa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'questao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.questao']"}),
            'texto': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'questionario.alternativacomposta': {
            'Meta': {'object_name': 'alternativaComposta'},
            'enunciado': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'questao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.questao']"})
        },
        u'questionario.alternativaobjetiva': {
            'Meta': {'object_name': 'alternativaObjetiva', '_ormbases': [u'questionario.alternativa']},
            'alternativa_composta': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.alternativaComposta']"}),
            u'alternativa_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['questionario.alternativa']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'questionario.alternativasubjetiva': {
            'Meta': {'object_name': 'alternativaSubjetiva', '_ormbases': [u'questionario.alternativa']},
            u'alternativa_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['questionario.alternativa']", 'unique': 'True', 'primary_key': 'True'}),
            'resposta': ('django.db.models.fields.TextField', [], {'max_length': '300'})
        },
        u'questionario.questao': {
            'Meta': {'object_name': 'questao'},
            'enunciado': ('django.db.models.fields.TextField', [], {'max_length': '300'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.session']"})
        },
        u'questionario.questionario': {
            'Meta': {'object_name': 'questionario'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'infor_Questionario': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            'nome_Questionario': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'questionario.session': {
            'Meta': {'object_name': 'session'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'infor_Session': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            'nome_Session': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'questionario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.questionario']"})
        }
    }

    complete_apps = ['questionario']