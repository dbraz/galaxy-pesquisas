# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'alternativaSubjetiva'
        db.delete_table(u'questionario_alternativasubjetiva')

        # Removing M2M table for field alternativa_composta on 'alternativaSubjetiva'
        db.delete_table(db.shorten_name(u'questionario_alternativasubjetiva_alternativa_composta'))

        # Adding field 'questao.responta_id'
        db.add_column(u'questionario_questao', 'responta_id',
                      self.gf('django.db.models.fields.CharField')(max_length=3, null=True, blank=True),
                      keep_default=False)

        # Removing M2M table for field alternativa_composta on 'alternativaObjetiva'
        db.delete_table(db.shorten_name(u'questionario_alternativaobjetiva_alternativa_composta'))


    def backwards(self, orm):
        # Adding model 'alternativaSubjetiva'
        db.create_table(u'questionario_alternativasubjetiva', (
            (u'alternativa_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['questionario.alternativa'], unique=True, primary_key=True)),
            ('resposta', self.gf('django.db.models.fields.TextField')(max_length=300)),
        ))
        db.send_create_signal(u'questionario', ['alternativaSubjetiva'])

        # Adding M2M table for field alternativa_composta on 'alternativaSubjetiva'
        m2m_table_name = db.shorten_name(u'questionario_alternativasubjetiva_alternativa_composta')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('alternativasubjetiva', models.ForeignKey(orm[u'questionario.alternativasubjetiva'], null=False)),
            ('questao', models.ForeignKey(orm[u'questionario.questao'], null=False))
        ))
        db.create_unique(m2m_table_name, ['alternativasubjetiva_id', 'questao_id'])

        # Deleting field 'questao.responta_id'
        db.delete_column(u'questionario_questao', 'responta_id')

        # Adding M2M table for field alternativa_composta on 'alternativaObjetiva'
        m2m_table_name = db.shorten_name(u'questionario_alternativaobjetiva_alternativa_composta')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('alternativaobjetiva', models.ForeignKey(orm[u'questionario.alternativaobjetiva'], null=False)),
            ('questao', models.ForeignKey(orm[u'questionario.questao'], null=False))
        ))
        db.create_unique(m2m_table_name, ['alternativaobjetiva_id', 'questao_id'])


    models = {
        u'questionario.alternativa': {
            'Meta': {'object_name': 'alternativa'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'index': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'questao': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.questao']", 'null': 'True', 'blank': 'True'}),
            'texto': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'questionario.alternativaobjetiva': {
            'Meta': {'object_name': 'alternativaObjetiva', '_ormbases': [u'questionario.alternativa']},
            u'alternativa_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['questionario.alternativa']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'questionario.questao': {
            'Meta': {'object_name': 'questao'},
            'enunciado': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'responta_id': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True', 'blank': 'True'}),
            'session': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.session']"})
        },
        u'questionario.questionario': {
            'Meta': {'object_name': 'questionario'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'infor_Questionario': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            'nome_Questionario': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'questionario.session': {
            'Meta': {'object_name': 'session'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'infor_Session': ('django.db.models.fields.TextField', [], {'max_length': '300', 'blank': 'True'}),
            'nome_Session': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'questionario': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['questionario.questionario']"})
        }
    }

    complete_apps = ['questionario']