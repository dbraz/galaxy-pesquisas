#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
import datetime

NIVEL_CHOICES = (

    (u'1', u'A'),(u'2', u'B'),(u'3', u'C'),(u'4', u'D'),(u'5', u'E')
)


#modelo que forma a construção do questionario
class questionario(models.Model):
	nome_Questionario = models.CharField(max_length=300)
	infor_Questionario = models.TextField(max_length=300,blank = True)
	status = models.BooleanField(default = False)
	def __unicode__(self):
		return self.nome_Questionario

class session(models.Model):
    questionario = models.ForeignKey(questionario)
    nome_Session = models.CharField(max_length=300)
    infor_Session = models.TextField(max_length=300,blank = True)
    def __unicode__(self):
        return self.nome_Session

class questao(models.Model):    
    session = models.ForeignKey(session)
    enunciado = models.TextField(max_length=300,blank = True)
    resposta_id = models.CharField(max_length=3,null = True,blank = True)
    def __unicode__(self):
        return self.enunciado

# class alternativaComposta(models.Model):
#     index = models.CharField(max_length=2)
#     enunciado = models.TextField(max_length=200,blank = True)
#     def __unicode__(self):
#         return self.enunciado

class alternativa(models.Model):
    questao = models.ForeignKey(questao,null = True,blank = True)
    index = models.CharField(max_length=3)
    texto = models.CharField(max_length=300)
    def __unicode__(self):
        return self.texto

# class alternativaSubjetiva(alternativa):
#     alternativa_composta = models.ManyToManyField(questao,null = True,blank = True)
#     resposta = models.TextField(max_length=300)

class alternativaObjetiva(alternativa):
    pass
    # alternativa_composta = models.ManyToManyField(questao,null = True,blank = True) 

# class subjetivaComposta(alternativa):
#     alternativa_composta = models.ForeignKey(alternativaComposta,null = True,blank = True)
#     resposta = models.TextField(max_length=300)    

# class objetivaComposta(alternativa):
#     alternativa_composta = models.ForeignKey(alternativaComposta,null = True,blank = True) 
#     enunciado = models.CharField(max_length=300)    qq