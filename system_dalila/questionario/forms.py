#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
# from django.db.models import Q
# from django.core.cache import cache
from models import *
def alternativas(questoes):
	# questoesSubjetiva = alternativaSubjetiva.objects.filter(questao = questoes)
	questoesObjetiva = alternativaObjetiva.objects.filter(questao = questoes)
	tag_list = []
	# tag_names = [tag.nomeProva for tag in prova.objects.all()]

	for name in questoesObjetiva:
		tag_list.append((name.texto,name.texto))

	return tag_list


class candidatoForm(forms.Form):
    usuario = forms.CharField(max_length=45,widget= forms.TextInput(attrs={ 'type':'text','class':"form-control", 'placeholder':'Usuário',}),label="")
    senha = forms.CharField(max_length=45,widget= forms.TextInput(attrs={ 'type':'password','class':"form-control", 'placeholder':'Senha',}),label="")

class questoesForm(forms.Form):
	def __init__(self,questao):
		super(forms.Form, self).__init__()
		self.questao = questao
	# print questao.enunciado
	# CHOICES = alternativas(questao)
	alternativas = forms.ChoiceField(label = '',choices=alternativas(questao), widget=forms.RadioSelect())
	resposta = forms.CharField(max_length=45,widget= forms.TextInput(attrs={ 'type':'text'}),label="")

		