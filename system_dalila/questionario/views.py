#!/usr/bin/env python
# coding=ISO-8859-1
from django.shortcuts import render
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from models import *
from forms import *
from django.db.models import Q
from django.contrib.auth.decorators import login_required, user_passes_test
import json
from django.core import serializers
import pandas  
from pandas import *
import sqlite3

def logar(request):
    form = candidatoForm()
    if request.method == 'POST':
        username = request.POST['usuario']
        password = request.POST['senha']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/teste/')
            else:
                msg = {'classe': 'alert-error', 'msg': 'Usuario desativado'}
                return render_to_response("login4.html", {'msg': msg,"forms":candidatoForm()},
                                          context_instance=RequestContext(request))
        else:
            msg = {'classe': 'alert-error', 'msg': 'Usuario desativado'}
            return render_to_response("login4.html", {'msg': msg,"forms":candidatoForm()},
                                      context_instance=RequestContext(request))
    else:
        msg = {'classe': 'alert-info', 'msg': 'Please login with your Username and Password.'}
        return render_to_response("login4.html", {'msg': msg,"forms":form},
                                  context_instance = RequestContext(request))


def desloga(request):
    logout(request)
    return HttpResponseRedirect('/logar/')


@login_required
def teste(request):

    # print len(lista_questao)
    if request.method == "POST":
        print request.POST
        print request.POST['cont1']
    if request.method == "GET":
        print request.GET
        quest = questionario.objects.all()
        # print quest
        if quest.filter(nome_Questionario = str(request.GET.get('parametros'))):
            sessao = session.objects.filter(questionario = quest.get(nome_Questionario = str(request.GET.get('parametros'))))
            questoes = questao.objects.filter(session = sessao)

            print len(questoes)
            lista_questao = []
            cont =0
            for quest in questoes:
                cont+=1
                a = questoesForm(quest)

                a.fields[str(cont)] = a.fields['alternativas']
                del a.fields['alternativas']
                a.fields[str(cont)].widget.attrs['id'] = str(cont)
                a.fields[str(cont)].choices = alternativas(quest)
                
                del a.fields['resposta']

                form_questao = {
                                'index':cont,
                                'enunciado':quest.enunciado,
                                'alter':a
                                }

                lista_questao.append(form_questao)


            return render_to_response("home.html",{"questionario":lista_questao,'nome':request.GET.get('parametros')}, context_instance=RequestContext(request))
        else:
            lista_questao = []
            # form_questao = {
            #             'enunciado':'questionario nao encontrado',
            #             'alter':' '

            #     }

            lista_questao.append(form_questao)


            return render_to_response("home.html",{"questionario":lista_questao}, context_instance=RequestContext(request))

    else:
        lista_questao = []
        form_questao = {
                    'enunciado':'questionario selecionado',
                    'alter':' '

            }

        lista_questao.append(form_questao)


        return render_to_response("home.html",{"questionario":lista_questao}, context_instance=RequestContext(request))
        cann = sqlite3.connect('db.sqlite3')
        query = "SELECT * from bonbeiro"

def questionarios(request):
    # if request.method == "POST" and request.is_ajax:
    quest = questionario.objects.all()
    # print quest
    if quest.filter(nome_Questionario = str(request.POST['nome'])):
        print "oioioi"
        sessao = session.objects.filter(questionario = quest.get(nome_Questionario = str(request.POST['nome'])))
        questoes = questao.objects.filter(session = sessao)

        print len(questoes)
        lista_questao = []
        cont =0

        for quest in questoes:
            
            questoesObjetiva = alternativa.objects.filter(questao = quest)

            alter = []
            for x in questoesObjetiva:
                a = {
                    "id":x.id,
                    "texto":x.texto,
                    "id_questao":x.questao.id

                    }
                alter.append(a)
            
            form_questao = {
                # 'index':cont,
                "id":quest.id,
                'enunciado':quest.enunciado,
                "resposta_id":quest.resposta_id,
                "resposta_texto":"",
                'alter':alter
                }
          
            lista_questao.append(form_questao)
        retorno = json.dumps({"totalQuest":len(questoes),'questionario':lista_questao,'nomeQuestionario':request.POST['nome']})
        return HttpResponse(retorno,mimetype = 'text/javascript')


def responder(request):
    print request.POST['.[questionario][0][resposta_id]']
    print request.POST['.[totalQuest]'].__class__
    b = int(request.POST['.[totalQuest]'])
    print b.__class__
    for indice in range(b):
        id_quest = request.POST['.[questionario]['+str(indice)+'][id]']
        print "id da questao: %s" %id_quest
        quest_atual = questao.objects.get(id = id_quest)
        quest_atual.resposta_id = request.POST['.[questionario]['+str(indice)+'][resposta_id]']
        quest_atual.save()

    return HttpResponse({"Luis":"Santos"},mimetype = 'text/javascript')

def te(request):
    return render_to_response("te.html",{},context_instance = RequestContext(request))
