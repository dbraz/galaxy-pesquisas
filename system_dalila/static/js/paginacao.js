function pagingMenor(atual,max,url){
  $('.pagination').html('');
  if(1 == parseInt(max)){

  }
  else{
    for (var i = 1; i <= max; i++){
      var atv;
      if(i == atual){
        atv="class='active'";
      }
      else{
        atv=''
      }
      //url.replace('**',i.toString())
      if(i>0){
        html="<li "+atv+" data-pos="+i+" data-url='"+url+"'><a class='pagi'>"+i+"</a></li>"
        $('.pagination').append(html);
      }
     
    }
  }
}

function paging(atual,anterior,max,url){
  // console.log("em paging a url é: "+url); 
  atual = parseInt(atual);
  anterior = parseInt(anterior);
  max = parseInt(max);

  if(atual < 6){
  
    repaginar(1,6,max,atual,url);
  }

  else if(atual>=6){

    if(atual > anterior){

      if(atual + 5 > max){

        if(atual == 1 || atual == 2){
          repaginar(1,max,max,atual,url);
        }
        else{
          repaginar(atual - 2 ,max,max,atual,url);
        }
        
      }

      else if(atual + 5 < max){
        if(atual == 1 || atual == 2){
            repaginar(1,atual+3,max,atual,url);
        }
        else{
            repaginar(atual-2,atual+3,max,atual,url);
        } 
      }

      else if(atual + 5 == max){

        repaginar(atual-2,atual+3,max,atual,url);
      }
    }

    else if(atual < anterior){
      if(atual - 5 > 1){
        if(atual + 3 > max){
          repaginar(atual - 2,max,max,atual,  url);
        }else{
          repaginar(atual - 2,atual+3,max,atual,  url);
        }
        
      }
      else if(atual - 5 < 1){
        repaginar(1,6,max,atual,url);
      }
      else if(atual-5 == 1){
        repaginar(atual-2,atual+3,max,atual,url);
      }
    }

    else if(atual == anterior){
      if(atual == anterior && atual == 1){
        repaginar(1,6,max,1,url)
      }
      
    }
  }
}
  
function repaginar(min,pages,max,ativa,url){
  // console.log("em repaginar retorno: "+min + '-' + pages + '-' + max + '-' + ativa + '-' + url);
  $('.pagination').html('');
  if(1 == parseInt(max)){

  }
  else{
    for (var i = min; i <= pages; i++){
      var atv;
      if(i == ativa){
        atv="class='active'";
      }
      else{
        atv=''
      }
      //url.replace('**',i.toString())
      if(i>0){
        html="<li "+atv+" data-pos="+i+" data-url='"+url+"'><a class='pagi'>"+i+"</a></li>"
        $('.pagination').append(html);
      }
     
    }
   
    if(parseInt(max) > 6 && parseInt(ativa)<parseInt(max)){
      
      $('.pagination').append("<li class='disabled'><a class='pagi'>...</a></li>");
      $('.pagination').append("<li data-pos="+i+" data-url='"+url+"'><a class='pagi'>"+max+"</a></li>");
    } 

    if(parseInt(ativa)>5){
      $('.pagination').empty();
      $('.pagination').append("<li data-pos="+i+" data-url='"+url+"'><a class='pagi'>"+1+"</a></li>");
      $('.pagination').append("<li class='disabled'><a class='pagi'>...</a></li>");
      for (var i = min; i <= pages; i++){
        var atv;
        if(i == ativa){
          atv="class='active'";
        }
        else{
          atv=''
        }
        //url.replace('**',i.toString())
        if(i>0){
          html="<li "+atv+" data-pos="+i+" data-url='"+url+"'><a class='pagi'>"+i+"</a></li>"
          $('.pagination').append(html);
        }
       
      }
      if(parseInt(pages)!=parseInt(max)){

        if(parseInt(max) > 7 && parseInt(ativa)<parseInt(max)){          
          $('.pagination').append("<li class='disabled'><a class='pagi'>...</a></li>");
          $('.pagination').append("<li data-pos="+i+" data-url='"+url+"'><a class='pagi'>"+max+"</a></li>");
        } 
      } 
    }
     
  };
  
}

function setaapage(url,clicada,atual){
  // console.log("em setaapage: ");
  // console.log("url da requisição sem alteração: "+url);
  nova_url = url.replace('**',clicada);
  // console.log("url da requisicao com alteração: "+nova_url);
  $.getJSON(nova_url,function(data){
    // console.log(data);
    insere(data['candidatos']);
    infos = data['infos'];
    infos = infos[0];
    tgeral(infos['total']);
    $('#teleitor').text(infos['eleitores']);
    $('#tvotos').text(infos['votos']);
    
    total = parseInt(infos['total']);
    var paginas = 0;
    paginas = parseInt(infos['pages']);
    // alert(paginas);
    if(paginas<=6){
      // alert("oioioi");
      pagingMenor(clicada,paginas,url);  
    }
    else{
      // alert("passou direto");
      paging(clicada,atual,paginas,url);
    };
    
    // console.log(infos['titulo']);
    if(infos['titulo']!= undefined){
      titulo(infos['titulo']);
    }
  });
}

function setCargo(c){
  if (c =='Prefeito 2'){
      c = '2º Colocado';
  }
  return c;
} 

function insere(data){
  $("#hor-minimalist-b tbody").html('');
  $.each(data,function(indice,item){
    $("#hor-minimalist-b tbody").append(
      "<tr>"+ 
      "<td>"+item['municipio'] +"</td>"+
      "<td>"+item['nome'] +"</td>"+
      "<td>"+setCargo(item['cargo']) +"</td>"+
      "<td class='col-eleitor'>"+item['eleitores'] +"</td>"+
      "<td><a href=tel:'"+item['telefone']+"'>"+item['telefone']+"</a></td>"+
      "<td>"+item['partido'] +"</td>"+
      "<td class='col-voto'>"+item['votos'] +"</td>"+
      "<td>"+item['governador'] +"</td>"+
      "<td>"+item['deputadoe'] +"</td>"+
      "<td>"+item['deputadof'] +"</td>"+
      "<td>"+item['senador'] +"</td>"+
      "<td><div class='btn-group btn-group-xs'><button type='button' title='Editar' class='btn btn-default'onclick=redit("+item['id']+") ><span class='glyphicon glyphicon-edit'></span></button></a><button title='Comentarios' type='button' data-id="+item['id']+" class='btn btn-default cmt'><span class='glyphicon glyphicon-info-sign'></span></button></div><td></tr>"
    );
  });
  $('#hor-minimalist-b').trigger('footable_redraw');
}


function titulo(data){
  $('.bread').html("<ul class='breadcrumb'></ul>");
  $.each(data,function(indice,item){
    $(".breadcrumb").append(
      "<li><a href='#'>"+item+"</a></li>"
    );
  });
}

function tgeral(t){
  if(t>1){
    $('.tgeral').html(infos['total'] + ' resultados');
  }else{
    $('.tgeral').html(infos['total'] + ' resultado');
  }
}


$('.footable').footable({
  breakpoints: {
    phone: 500,
    tablet: 700
  }
});

$( ".buscar" ).click(function() {
  //toString()
  $("#hor-minimalist-b tbody").html("<tr id='loading' ><td colspan='12'><center><img src='/static/img/ring.gif' >Carregando</center></td></tr>");
  pars=decodeURIComponent($("#formbusca").serialize());
  pars =pars.toString()
  //console.log(pars);
  // setaapage("/json/?"+pars+,1,1);
   setaapage("/json/?"+pars+"&page=**",1,1);
});


$('.limpar').click(function(){

  $('.selectpicker').selectpicker('deselectAll');
});

//filtors

$('.filter-status').change(function (e) {
  e.preventDefault();
  $('table.footable').trigger('footable_filter', {filter: $('#filter').val()});
});


$('.clear-filter').click(function (e) {
  e.preventDefault();
  $('.filter-status').val('');
  $('table.footable').trigger('footable_clear_filter');
});


$('.sort-column').click(function (e) {
  e.preventDefault();

  //get the footable sort object
  var footableSort = $('table').data('footable-sort');

  //get the index we are wanting to sort by
  var index = $(this).data('index');

  footableSort.doSort(index, 'toggle');
});


$('#hor-minimalist-b tbody').on('click','.cmt',function(){ 
 id = $(this).data("id");
 // console.log(id);
 $("#cand").val(id);
 setaComent(id);
 $('#myModal').modal('show');
});

function limpaForm(){ 
$('textarea#obs').val('');
$('#prazo-inpt').val('');
$('#sel-situation').selectpicker('deselectAll');
$('#comentario').val('');
}

function setaLabel(label,id){ 
if(label=='Resolvido'){ 
    return "<span id='sit-"+id+"' class='label label-success'>Resolvido</span>";
  }else if(label=='Encaminhado'){
    return "<span id='sit-"+id+"' class='label label-warning'>Encaminhado</span>";
  }else if(label=='Pendente'){
  return "<span id='sit-"+id+"' class='label label-danger'>Não resolvido</span>";
}
}


$("#btn-add-coment").click(function(){    

if ($('textarea#obs').val()==''){ 
  $('textarea#obs').focus();
}else if($('#prazo-inpt').val()==''){ 
  $('#prazo-inpt').focus();
}else if($("#sel-situation").val()==''){ 
  $("#sel-situation").focus();
}else{


$.ajax({
  url :/comenta/,
  type : "POST",
  dataType: "json",
  contentType: "application/x-www-form-urlencoded; charset=UTF-8",
  data : {
    'csrfmiddlewaretoken': '{{ csrf_token }}',
    'obs': $('textarea#obs').val(),
    'candidato': $('#cand').val(),
    'prazo': $('#prazo-inpt').val(),
    'situacao': $("#sel-situation").val()
  },
  success : function(json) {
    // console.log('entrou');
      if(json['flag']=='Salvo'){ 
        limpaForm();
        setaComent();
      }else if(json['flag']=='Editado'){ 
        // console.log('Editado');
        limpaForm();
        setaComent();
      }else if(json['flag']=='Erro'){ 
        //console.log('Erro');
  }}
});}
});


function setaComent(id){ 
  $('#coments').html('');
  $.getJSON('/comentarios/'+id+'/',function(data){
    $.each(data['coments'],function(indice,item){
    $('#coments').prepend("<div class='comentario'><div class='label-coment'>"+setaLabel(item['situacao'],item['id']) +"<div style= 'float:right;'><button type='button' data-id="+item['id']+" class='btn edt btn-default btn-xs'><span class='glyphicon glyphicon-edit'></span></button><button type='button' data-id="+item['id']+" class='del btn btn-default btn-xs'><span class='glyphicon glyphicon-remove'></span></button></div></div><div id=crp-"+item['id']+" class='crp'>"+item['obs']+"</div><div id=prazo-"+item['id']+" class='rdpc'> "+item['prazo'] +"</div></div>");
  });
    });

}

function converted(d){ 
nd = d[7]+d[8]+d[9]+d[10]+'-'+d[4]+d[5]+'-'+d[1]+d[2];
return nd;
}

$('#coments').on('click','.edt',function(){ 
  id = $(this).data("id");
  obs = $('#coments').find('#crp-'+id).html();
  prazo = $('#coments').find('#prazo-'+id).html();
  sit = $('#coments').find('#sit-'+id).html();

  //console.log(id);
  //console.log(sit);
  $('#sel-situation').selectpicker('val', sit);
  $('textarea#obs').val(obs);
  $('#comentario').val(id);
  $('#prazo-inpt').val(converted(prazo));
});

function redit(id){ 

  window.location = "/edita/"+id+"/";
}

$("ul.pagination").on('click', 'li', function(){
  var clicada = $(this).find("a").text();
  var atual = $( ".active" ).find("a").text();
  var pos = $(this).data("pos");
  var url = $(this).data("url");
  $( ".active" ).removeClass( "active" );
  
  if($( this).hasClass( "active" )){
  }else{
      $(this).addClass( "active");
  }
  setaapage(url,clicada,atual);

});

$(".gerar").click(function(){

    $("#formbusca").submit();
});
